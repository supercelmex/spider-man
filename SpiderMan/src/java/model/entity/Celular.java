/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ulise
 */
@Entity
@Table(name = "celular")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Celular.findAll", query = "SELECT c FROM Celular c")
    , @NamedQuery(name = "Celular.findById", query = "SELECT c FROM Celular c WHERE c.id = :id")
    , @NamedQuery(name = "Celular.findByModelo", query = "SELECT c FROM Celular c WHERE c.modelo = :modelo")
    , @NamedQuery(name = "Celular.findBySo", query = "SELECT c FROM Celular c WHERE c.so = :so")
    , @NamedQuery(name = "Celular.findByPesoKg", query = "SELECT c FROM Celular c WHERE c.pesoKg = :pesoKg")
    , @NamedQuery(name = "Celular.findByPrecio", query = "SELECT c FROM Celular c WHERE c.precio = :precio")
    , @NamedQuery(name = "Celular.findByBateria", query = "SELECT c FROM Celular c WHERE c.bateria = :bateria")
    , @NamedQuery(name = "Celular.findByRam", query = "SELECT c FROM Celular c WHERE c.ram = :ram")
    , @NamedQuery(name = "Celular.findByAlmacenamientoInterno", query = "SELECT c FROM Celular c WHERE c.almacenamientoInterno = :almacenamientoInterno")
    , @NamedQuery(name = "Celular.findByProcesador", query = "SELECT c FROM Celular c WHERE c.procesador = :procesador")
    , @NamedQuery(name = "Celular.findByTama\u00f1oPantalla", query = "SELECT c FROM Celular c WHERE c.tama\u00f1oPantalla = :tama\u00f1oPantalla")
    , @NamedQuery(name = "Celular.findByResolucionPantalla", query = "SELECT c FROM Celular c WHERE c.resolucionPantalla = :resolucionPantalla")
    , @NamedQuery(name = "Celular.findByResolucionCamaraDelatera", query = "SELECT c FROM Celular c WHERE c.resolucionCamaraDelatera = :resolucionCamaraDelatera")
    , @NamedQuery(name = "Celular.findByResolucionCamaraTrasera", query = "SELECT c FROM Celular c WHERE c.resolucionCamaraTrasera = :resolucionCamaraTrasera")
    , @NamedQuery(name = "Celular.findByDescripcion", query = "SELECT c FROM Celular c WHERE c.descripcion = :descripcion")
    , @NamedQuery(name = "Celular.findByDescuento", query = "SELECT c FROM Celular c WHERE c.descuento = :descuento")
    , @NamedQuery(name = "Celular.findByCantidad", query = "SELECT c FROM Celular c WHERE c.cantidad = :cantidad")
    , @NamedQuery(name = "Celular.findByImage", query = "SELECT c FROM Celular c WHERE c.image = :image")
    , @NamedQuery(name = "Celular.findByEstatus", query = "SELECT c FROM Celular c WHERE c.estatus = :estatus")})
public class Celular implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    //@Size(min = 1, max = 50)
    @Column(name = "modelo",length = 50)
    private String modelo;
    @Basic(optional = false)
    @NotNull
    //@Size(min = 1, max = 25)
    @Column(name = "so",length = 25)
    private String so;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "peso_kg")
    private float pesoKg;
    @Basic(optional = false)
    @NotNull
    @Column(name = "precio")
    private float precio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "bateria")
    private int bateria;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ram")
    private int ram;
    @Basic(optional = false)
    @NotNull
    @Column(name = "almacenamiento_interno")
    private int almacenamientoInterno;
    //@Size(max = 25)
    @Column(name = "procesador",length = 25)
    private String procesador;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tama\u00f1o_pantalla")
    private float tamañoPantalla;
    @Column(name = "resolucion_pantalla")
    private float resolucionPantalla;
    @Column(name = "resolucion_camara_delatera")
    private float resolucionCamaraDelatera;
    @Column(name = "resolucion_camara_trasera")
    private float resolucionCamaraTrasera;
    @Basic(optional = false)
    @NotNull
    //@Size(min = 1, max = 50)
    @Column(name = "descripcion",length = 50)
    private String descripcion;
    @Column(name = "descuento")
    private float descuento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cantidad")
    private int cantidad;
    //@Size(max = 100)
    @Column(name = "image",length = 100)
    private String image;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estatus")
    private boolean estatus;
    @JoinColumn(name = "id_marca", referencedColumnName = "id")
    @ManyToOne
    private Marca idMarca;
    @OneToMany(mappedBy = "idCelular")
    private List<DetVen> detVenList;

    public Celular() {
    }

    public Celular(Integer id) {
        this.id = id;
    }

    public Celular(Integer id, String modelo, String so, float precio, int bateria, int ram, int almacenamientoInterno, float tamañoPantalla, String descripcion, int cantidad, boolean estatus) {
        this.id = id;
        this.modelo = modelo;
        this.so = so;
        this.precio = precio;
        this.bateria = bateria;
        this.ram = ram;
        this.almacenamientoInterno = almacenamientoInterno;
        this.tamañoPantalla = tamañoPantalla;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
        this.estatus = estatus;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getSo() {
        return so;
    }

    public void setSo(String so) {
        this.so = so;
    }

    public float getPesoKg() {
        return pesoKg;
    }

    public void setPesoKg(float pesoKg) {
        this.pesoKg = pesoKg;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public int getBateria() {
        return bateria;
    }

    public void setBateria(int bateria) {
        this.bateria = bateria;
    }

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public int getAlmacenamientoInterno() {
        return almacenamientoInterno;
    }

    public void setAlmacenamientoInterno(int almacenamientoInterno) {
        this.almacenamientoInterno = almacenamientoInterno;
    }

    public String getProcesador() {
        return procesador;
    }

    public void setProcesador(String procesador) {
        this.procesador = procesador;
    }

    public float getTamañoPantalla() {
        return tamañoPantalla;
    }

    public void setTamañoPantalla(float tamañoPantalla) {
        this.tamañoPantalla = tamañoPantalla;
    }

    public float getResolucionPantalla() {
        return resolucionPantalla;
    }

    public void setResolucionPantalla(float resolucionPantalla) {
        this.resolucionPantalla = resolucionPantalla;
    }

    public float getResolucionCamaraDelatera() {
        return resolucionCamaraDelatera;
    }

    public void setResolucionCamaraDelatera(float resolucionCamaraDelatera) {
        this.resolucionCamaraDelatera = resolucionCamaraDelatera;
    }

    public float getResolucionCamaraTrasera() {
        return resolucionCamaraTrasera;
    }

    public void setResolucionCamaraTrasera(float resolucionCamaraTrasera) {
        this.resolucionCamaraTrasera = resolucionCamaraTrasera;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getDescuento() {
        return descuento;
    }

    public void setDescuento(float descuento) {
        this.descuento = descuento;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean getEstatus() {
        return estatus;
    }

    public void setEstatus(boolean estatus) {
        this.estatus = estatus;
    }

    public Marca getIdMarca() {
        return idMarca;
    }

    public void setIdMarca(Marca idMarca) {
        this.idMarca = idMarca;
    }

    @XmlTransient
    public List<DetVen> getDetVenList() {
        return detVenList;
    }

    public void setDetVenList(List<DetVen> detVenList) {
        this.detVenList = detVenList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Celular)) {
            return false;
        }
        Celular other = (Celular) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.entity.Celular[ id=" + id + " ]";
    }
    
}
