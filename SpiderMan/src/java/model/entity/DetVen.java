/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ulise
 */
@Entity
@Table(name = "det_ven")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetVen.findAll", query = "SELECT d FROM DetVen d")
    , @NamedQuery(name = "DetVen.findById", query = "SELECT d FROM DetVen d WHERE d.id = :id")
    , @NamedQuery(name = "DetVen.findByCantidad", query = "SELECT d FROM DetVen d WHERE d.cantidad = :cantidad")})
public class DetVen implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "cantidad")
    private Integer cantidad;
    @JoinColumn(name = "id_celular", referencedColumnName = "id")
    @ManyToOne
    private Celular idCelular;
    @JoinColumn(name = "id_venta", referencedColumnName = "id")
    @ManyToOne
    private Venta idVenta;

    public DetVen() {
    }

    public DetVen(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Celular getIdCelular() {
        return idCelular;
    }

    public void setIdCelular(Celular idCelular) {
        this.idCelular = idCelular;
    }

    public Venta getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(Venta idVenta) {
        this.idVenta = idVenta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetVen)) {
            return false;
        }
        DetVen other = (DetVen) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.entity.DetVen[ id=" + id + " ]";
    }
    
}
