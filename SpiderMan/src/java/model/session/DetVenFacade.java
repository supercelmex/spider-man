/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import model.entity.DetVen;

/**
 *
 * @author ulise
 */
@Stateless
public class DetVenFacade extends AbstractFacade<DetVen> {

    @PersistenceContext(unitName = "SpiderManPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DetVenFacade() {
        super(DetVen.class);
    }
    
}
