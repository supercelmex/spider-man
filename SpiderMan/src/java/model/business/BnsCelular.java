/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.business;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.entity.Celular;
import model.session.CelularFacade;

/**
 *
 * @author ulise
 */
@Stateless
public class BnsCelular {
    
    @PersistenceContext(unitName = "SpiderManPU")
    private EntityManager em;
    
    @EJB
    private CelularFacade celularF;

    public List<Celular> listadoCelularesPorEstatus(boolean estatus) {
        Query query = null;
        query = em.createQuery("SELECT c FROM Celular c WHERE c.estatus = :estatus");
        query.setParameter("estatus", estatus);

        return query.getResultList();
    }
    
     public void cambioDeEstatu(int id, boolean estatus) {
        Celular c = celularF.find(id);
        c.setEstatus(estatus);
        celularF.edit(c);

    }
     public List<Celular> verificarCelular(String modelo){
        Query query = null;
        query = em.createQuery("SELECT c.id FROM Celular c WHERE c.modelo like :modelo");
        query.setParameter("modelo" ,modelo);
        
     
        return query.getResultList();
     }
     
}
