/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.business;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.entity.Marca;
import model.session.MarcaFacade;

/**
 *
 * @author ulise
 */
@Stateless
public class BnsMarca {
    
    @PersistenceContext(unitName = "SpiderManPU")
    private EntityManager em;
    
    @EJB
    private MarcaFacade marcaF;

    public List<Marca> listadoMarcasPorEstatus(boolean estatus) {
        Query query = null;
        query = em.createQuery("SELECT m FROM Marca m WHERE m.estatus = :estatus");
        query.setParameter("estatus", estatus);

        return query.getResultList();
    }

    public void cambioDeEstatusMarca(int idMarca, boolean estatus) {
        Marca m = marcaF.find(idMarca);
        m.setEstatus(estatus);
        marcaF.edit(m);

    }
    
    public void insetar(Marca marca){
      em.createNamedQuery("INSERT INTO marca (nombre,descuento,estatus,descripcion) VALUES (?,?,?,?)")
              .setParameter(1, marca.getNombre())
              .setParameter(2, marca.getDescuento())
              .setParameter(3, true)
              .setParameter(4, marca.getDescripcion())
              .executeUpdate();
    }
    
    public List<Marca> listadoMarcasNot(int idMarca) {
        Query query = null;
        query = em.createQuery("SELECT m FROM Marca m WHERE m.estatus = :estatus and m.id != :id");
        query.setParameter("estatus", true);
        query.setParameter("id", idMarca);

        return query.getResultList();
    }
     
    public List<Marca> verificarMarca(String nombre) {
        Query query = null;
        query = em.createQuery("SELECT m.id FROM Marca m WHERE m.nombre like :nombre");
        query.setParameter("nombre" ,nombre);
     
        return query.getResultList();

    }

}
