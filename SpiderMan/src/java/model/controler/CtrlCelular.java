/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.controler;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.business.BnsCelular;
import model.business.BnsMarca;
import model.entity.Celular;
import model.entity.Marca;
import model.session.CelularFacade;
import model.session.MarcaFacade;

/**
 *
 * @author ulise
 */
@WebServlet(name = "CtrlCelular", urlPatterns = {"/celulares", "/mc", "/ac", "/ec", "/celularesOn"})
public class CtrlCelular extends HttpServlet {

    @EJB
    private CelularFacade celularF;
    @EJB
    private BnsCelular celularB;
    @EJB
    private BnsMarca marcaB;
    @EJB
    private MarcaFacade marcaF;

    /**
     * /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CtrlCelular</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CtrlCelular at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = request.getServletPath();
        List<Celular> celularesOn = new ArrayList<>();
        List<Celular> celularesOff = new ArrayList<>();
        List<Marca> marcas = new ArrayList<>();

        Celular celular;
        switch (url) {
            case "/celulares":
                celularesOn = celularB.listadoCelularesPorEstatus(true);
                celularesOff = celularB.listadoCelularesPorEstatus(false);
                request.setAttribute("celularesOn", celularesOn);
                request.setAttribute("celularesOff", celularesOff);
                request.setAttribute("titulo", "Lista de celulares");
                request.getRequestDispatcher("WEB-INF/views/celulares.jsp").forward(request, response);
                break;
            case "/ac":
                marcas = marcaB.listadoMarcasPorEstatus(true);
                request.setAttribute("titulo", "Agregar celular");
                request.setAttribute("action", "ac");
                request.setAttribute("m", marcas);
                request.getRequestDispatcher("WEB-INF/views/celular-form.jsp").forward(request, response);

                break;
            case "/ec":
                int idCelular = Integer.parseInt(request.getQueryString());
                celular = celularF.find(idCelular);
                marcas = marcaB.listadoMarcasNot(celular.getIdMarca().getId());
                request.setAttribute("c", celular);
                request.setAttribute("titulo", "Editar");
                request.setAttribute("action", "ec");
                request.setAttribute("m", marcas);
                request.getRequestDispatcher("WEB-INF/views/celular-form.jsp").forward(request, response);
                break;

        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = request.getServletPath();
        List<Celular> celularesOn = new ArrayList<>();
        List<Celular> celularesV = new ArrayList<>();
        Celular celular = null;
        switch (url) {
            case "/mc":
                if (request.getParameter("estatus").equals("1")) {
                    celularB.cambioDeEstatu(Integer.parseInt(request.getParameter("id")), false);
                } else {
                    celularB.cambioDeEstatu(Integer.parseInt(request.getParameter("id")), true);
                }
                break;
            case "/ac":
                String modelo = request.getParameter("modelo");
                celularesV = celularB.verificarCelular(modelo);
                if (celularesV.size() == 0) {
                    celular = new Celular();
                    celular.setModelo(request.getParameter("modelo"));
                        celular.setSo(request.getParameter("so"));
                        celular.setPesoKg(Float.parseFloat(request.getParameter("peso").equals(null) ? "0" : request.getParameter("peso")));
                        celular.setPrecio(Float.parseFloat(request.getParameter("precio").equals(null) ? "0" : request.getParameter("precio")));
                        celular.setBateria(Integer.parseInt(request.getParameter("bateria").equals(null) ? "0" : request.getParameter("bateria")));
                        celular.setRam(Integer.parseInt(request.getParameter("ram")));
                        celular.setAlmacenamientoInterno(Integer.parseInt(request.getParameter("almacenamiento")));
                        celular.setProcesador(request.getParameter("procesador"));
                        celular.setTamañoPantalla(Float.parseFloat(request.getParameter("tamano").equals(null) ? "0" : request.getParameter("tamano")));
                        celular.setResolucionPantalla(Float.parseFloat(request.getParameter("resolucion").equals(null) ? "0" : request.getParameter("resolucion")));
                        celular.setResolucionCamaraDelatera(Float.parseFloat(request.getParameter("resDel").equals(null) ? "0" : request.getParameter("resDel")));
                        celular.setResolucionCamaraTrasera(Float.parseFloat(request.getParameter("resTras").equals(null) ? "0" : request.getParameter("resTras")));
                        celular.setDescripcion(request.getParameter("descripcion"));
                        celular.setDescuento(Float.parseFloat(request.getParameter("descuento") == null ? "0" : request.getParameter("descuento")));
                        celular.setCantidad(Integer.parseInt(request.getParameter("cantidad").equals(null) ? "0" : request.getParameter("cantidad")));
                        celular.setIdMarca(marcaF.find(Integer.parseInt(request.getParameter("idMarca"))));
                        celular.setImage(null);
                    celular.setEstatus(true);
                    celularF.create(celular);
                    response.sendRedirect("ac");
                } else {
                    request.setAttribute("error", "error");
                    request.setAttribute("errorDesc", "Modelo duplicada");
                    request.getRequestDispatcher("WEB-INF/views/celular-form.jsp").forward(request, response);

                }

                break;
            case "/ec":
                String modelo2 = request.getParameter("modelo");
                celular = celularF.find(Integer.parseInt(request.getParameter("id")));

                if (!celular.getModelo().equals(modelo2)) {
                    celularesV = celularB.verificarCelular(modelo2);
                    if (celularesV.size() > 0) {
                        request.setAttribute("error", "error");
                        request.setAttribute("errorDesc", "Modelo duplicada");
                        request.getRequestDispatcher("WEB-INF/views/celular-form.jsp").forward(request, response);

                    } else {
                        celular.setModelo(request.getParameter("modelo"));
                        celular.setSo(request.getParameter("so"));
                        celular.setPesoKg(Float.parseFloat(request.getParameter("peso").equals(null) ? "0" : request.getParameter("peso")));
                        celular.setPrecio(Float.parseFloat(request.getParameter("precio").equals(null) ? "0" : request.getParameter("precio")));
                        celular.setBateria(Integer.parseInt(request.getParameter("bateria").equals(null) ? "0" : request.getParameter("bateria")));
                        celular.setRam(Integer.parseInt(request.getParameter("ram")));
                        celular.setAlmacenamientoInterno(Integer.parseInt(request.getParameter("almacenamiento")));
                        celular.setProcesador(request.getParameter("procesador"));
                        celular.setTamañoPantalla(Float.parseFloat(request.getParameter("tamano").equals(null) ? "0" : request.getParameter("tamano")));
                        celular.setResolucionPantalla(Float.parseFloat(request.getParameter("resolucion").equals(null) ? "0" : request.getParameter("resolucion")));
                        celular.setResolucionCamaraDelatera(Float.parseFloat(request.getParameter("resDel").equals(null) ? "0" : request.getParameter("resDel")));
                        celular.setResolucionCamaraTrasera(Float.parseFloat(request.getParameter("resTras").equals(null) ? "0" : request.getParameter("resTras")));
                        celular.setDescripcion(request.getParameter("descripcion"));
                        celular.setDescuento(Float.parseFloat(request.getParameter("descuento") == null ? "0" : request.getParameter("descuento")));
                        celular.setCantidad(Integer.parseInt(request.getParameter("cantidad").equals(null) ? "0" : request.getParameter("cantidad")));
                        celular.setIdMarca(marcaF.find(Integer.parseInt(request.getParameter("idMarca"))));
                        celular.setImage(null);
                        celularF.edit(celular);
                        response.sendRedirect("celulares");
                    }
                } else {
                    celular.setModelo(request.getParameter("modelo"));
                        celular.setSo(request.getParameter("so"));
                        celular.setPesoKg(Float.parseFloat(request.getParameter("peso").equals(null) ? "0" : request.getParameter("peso")));
                        celular.setPrecio(Float.parseFloat(request.getParameter("precio").equals(null) ? "0" : request.getParameter("precio")));
                        celular.setBateria(Integer.parseInt(request.getParameter("bateria").equals(null) ? "0" : request.getParameter("bateria")));
                        celular.setRam(Integer.parseInt(request.getParameter("ram")));
                        celular.setAlmacenamientoInterno(Integer.parseInt(request.getParameter("almacenamiento")));
                        celular.setProcesador(request.getParameter("procesador"));
                        celular.setTamañoPantalla(Float.parseFloat(request.getParameter("tamano").equals(null) ? "0" : request.getParameter("tamano")));
                        celular.setResolucionPantalla(Float.parseFloat(request.getParameter("resolucion").equals(null) ? "0" : request.getParameter("resolucion")));
                        celular.setResolucionCamaraDelatera(Float.parseFloat(request.getParameter("resDel").equals(null) ? "0" : request.getParameter("resDel")));
                        celular.setResolucionCamaraTrasera(Float.parseFloat(request.getParameter("resTras").equals(null) ? "0" : request.getParameter("resTras")));
                        celular.setDescripcion(request.getParameter("descripcion"));
                        celular.setDescuento(Float.parseFloat(request.getParameter("descuento") == null ? "0" : request.getParameter("descuento")));
                        celular.setCantidad(Integer.parseInt(request.getParameter("cantidad").equals(null) ? "0" : request.getParameter("cantidad")));
                        celular.setIdMarca(marcaF.find(Integer.parseInt(request.getParameter("idMarca"))));
                        celular.setImage(null);
                    celularF.edit(celular);
                    response.sendRedirect("celulares");
                }

                break;
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
