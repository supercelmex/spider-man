/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.controler;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.business.BnsCelular;
import model.business.BnsMarca;
import model.entity.Celular;
import model.entity.Marca;
import model.session.CelularFacade;
import model.session.MarcaFacade;

/**
 *
 * @author ulise
 */
@WebServlet(name = "CtrlTienda", urlPatterns = {"/CtrlTienda","/login","/tienda","/vc"})
public class CtrlTienda extends HttpServlet {
    
    @EJB
    private MarcaFacade marcaF;
    @EJB
    private BnsMarca marcaB;
    @EJB
    private CelularFacade celularF;
    @EJB
    private BnsCelular celularB;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CtrlTienda</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CtrlTienda at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       // processRequest(request, response);
       String url = request.getServletPath();
       List<Marca> marcas = new ArrayList<>();
       List<Celular> celulares = new ArrayList<>();
       Celular celular;
       switch(url)
       {
           case "/tienda":
               //celulares = celularB.listadoCelularesPorEstatus(true);
               marcas = marcaB.listadoMarcasPorEstatus(true);
               request.setAttribute("marcas", marcas);
               request.getRequestDispatcher("WEB-INF/views/tienda.jsp").forward(request, response);
               break;
           case "/login":
               request.getRequestDispatcher("WEB-INF/views/login.jsp").forward(request, response);
               break;
               
           case "/vc":
               int idCelular = Integer.parseInt(request.getQueryString());
                celular = celularF.find(idCelular);
                request.setAttribute("c", celular);
                 request.getRequestDispatcher("WEB-INF/views/producto.jsp").forward(request, response);
               break;
       }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
