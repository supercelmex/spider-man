/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.controler;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.business.BnsMarca;
import model.entity.Marca;
import model.session.MarcaFacade;

/**
 *
 * @author ulise
 */
@WebServlet(name = "CtrlMarca", urlPatterns = {"/marcas", "/mm","/am","/em"})
public class CtrlMarca extends HttpServlet {

    @EJB
    private MarcaFacade marcaF;
    @EJB
    private BnsMarca marcaB;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CtrlMarca</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CtrlMarca at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = request.getServletPath();
        List<Marca> marcasOn = new ArrayList<>();
        List<Marca> marcasOff = new ArrayList<>();
        String strIdMarca = null;
        Marca marca;
        switch (url) {
            case "/marcas":
                marcasOn = marcaB.listadoMarcasPorEstatus(true);
                marcasOff = marcaB.listadoMarcasPorEstatus(false);
                request.setAttribute("marcasOn", marcasOn);
                request.setAttribute("marcasOff", marcasOff);
                request.setAttribute("titulo", "Lista de marcas");
                request.getRequestDispatcher("WEB-INF/views/marcas.jsp").forward(request, response);
                break;
            case "/am":

               request.setAttribute("titulo", "Agregar marca");
               request.setAttribute("action", "am");
               request.getRequestDispatcher("WEB-INF/views/marca-form.jsp").forward(request, response);
                break;
            case "/em":
                int idMarca = Integer.parseInt(request.getQueryString());
                marca  = marcaF.find(idMarca);
                request.setAttribute("m", marca);
                request.setAttribute("titulo", "Editar");
                request.setAttribute("action", "em");
                request.getRequestDispatcher("WEB-INF/views/marca-form.jsp").forward(request, response);
               
                break;

        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = request.getServletPath();
        List<Marca> marcaV = new ArrayList<>();
        Marca marca = null;
        try {
            switch (url) {
                case "/mm":
                    if (request.getParameter("estatusMarca").equals("1")) {
                        marcaB.cambioDeEstatusMarca(Integer.parseInt(request.getParameter("idMarca")),false);
                    } else {
                        marcaB.cambioDeEstatusMarca(Integer.parseInt(request.getParameter("idMarca")),true);                        
                    }
                    break;
                case "/am":
                    String nombre = request.getParameter("nombre");
                    marcaV = marcaB.verificarMarca(nombre);
                    if (marcaV.size() == 0) {
                        marca = new Marca();
                        marca.setNombre(request.getParameter("nombre"));
                        marca.setDescripcion(request.getParameter("descripcion"));
                        marca.setDescuento(Float.parseFloat(request.getParameter("descuento")));
                        marca.setEstatus(Boolean.TRUE);
                        //marcaB.insetar(marca);
                        marcaF.create(marca);
                        response.sendRedirect("am");
                        
                    } else {
                        request.setAttribute("error", "error");
                        request.setAttribute("errorDesc", "Nombre duplicada");
                        request.getRequestDispatcher("WEB-INF/views/marca-form.jsp").forward(request, response);
                    }

                    break;
                case "/em":
                    String nombre2 = request.getParameter("nombre");
                    marca = marcaF.find(Integer.parseInt(request.getParameter("id")));
                    if (!marca.getNombre().equals(nombre2)) {
                        marcaV = marcaB.verificarMarca(nombre2);
                        if (marcaV.size() > 0) {
                            request.setAttribute("error", "error");
                            request.setAttribute("errorDesc", "Marca duplicada");
                            request.getRequestDispatcher("WEB-INF/views/marca-form.jsp").forward(request, response);
                        } else {
                            marca.setNombre(request.getParameter("nombre"));
                            marca.setDescripcion(request.getParameter("descripcion"));
                            marca.setDescuento(Float.parseFloat(request.getParameter("descuento")));
                            marcaF.edit(marca);
                            response.sendRedirect("marcas");
                        }
                    } else {
                        marca.setNombre(request.getParameter("nombre"));
                        marca.setDescripcion(request.getParameter("descripcion"));
                        marca.setDescuento(Float.parseFloat(request.getParameter("descuento")));
                        marcaF.edit(marca);
                        response.sendRedirect("marcas");
                    }

                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
