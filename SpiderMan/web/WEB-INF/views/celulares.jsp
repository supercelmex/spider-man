<%-- 
    Document   : celulares
    Created on : 6/08/2019, 07:28:00 PM
    Author     : ulise
--%>

  <main role="main" class="container">
        <div class="d-flex align-items-center p-3 my-3 text-gray-dark bg-purple rounded shadow-sm">
            <h1> ${titulo}
                <input type="button" class="btn btn-success" value="+" name="btnAgregar" onclick="location.href = 'ac'" />
            </h1>
        </div>
        <h6 class="border-bottom border-gray pb-2 mb-0">Celulares activas</h6>


        <c:forEach var="c" items="${celularesOn}">
            <div class="my-3 p-3 bg-white rounded shadow-sm">
                <div class="media text-muted pt-3">
                    <svg class="bd-placeholder-img mr-2 rounded" width="32" height="32" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 32x32"><title>Placeholder</title><rect width="100%" height="100%" fill="#007bff"/><text x="50%" y="50%" fill="#ffffff" dy=".3em">${c.id}</text></svg>
                    <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">${c.modelo}</strong>
                        <td>${c.precio}</td>
                        <td>${c.cantidad}</td>
                    </p>
                </div>
                <small class="d-block text-right mt-3">
                    <input class="btn btn-light"  type="button" value="Editar" name="btnFromEditar" onclick="location.href = 'ec?${c.id}'"/> 
                    <input class="btn" style="background-color: #E76144" type="button" value="Borrar" name="btnFromEBorrar" onclick='borrarCel(${c.id},1)'/> 

                </small>
            </div>        
        </c:forEach>
         <h6 class="border-bottom border-gray pb-2 mb-0">Marcas inactivas</h6>


        <c:forEach var="cOff" items="${celularesOff}">
            <div class="my-3 p-3 bg-white rounded shadow-sm">
                <div class="media text-muted pt-3">
                    <svg class="bd-placeholder-img mr-2 rounded" width="32" height="32" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 32x32"><title>Placeholder</title><rect width="100%" height="100%" fill="#E76144"/><text x="50%" y="50%" fill="#ffffff" dy=".3em">${cOff.id}</text></svg>
                    <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">${cOff.modelo}</strong>
                        <td>${cOff.precio}</td>
                        <td>${cOff.cantidad}</td>
                    </p>
                </div>
                <small class="d-block text-right mt-3">                  
                    <input class="btn" style="background-color: #5FD759" type="button" value="Activar" name="btnFromEBorrar" onclick='borrarCel(${cOff.id},0)'/> 

                </small>
            </div>        
        </c:forEach>
    </main>
