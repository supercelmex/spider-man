/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



function borrar(id, estatus) {
    if (confirm('Deseas borrar?')) {
        $.ajax({
            url: 'mm',
            type: 'POST',
            dataType: 'text',
            data: {idMarca: id, estatusMarca: estatus},
            success: function (data) {
                location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('error');
            }
        });
    }
}
function borrar2(id, estatus) {
    swal({
        title: "SUPER CEL MX",
        text: "¿Estás seguro de realizar el cambio?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: 'mm',
                        type: 'POST',
                        dataType: 'text',
                        data: {idMarca: id, estatusMarca: estatus},
                        success: function (data) {
                            location.reload()

                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            swal("¡Algo salió mal!", {
                                icon: "error",
                            });
                        }
                    });

                } else {

                }
            });

}

function borrarCel(id, estatus) {
    swal({
        title: "SUPER CEL MX",
        text: "¿Estás seguro de realizar el cambio?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: 'mc',
                        type: 'POST',
                        dataType: 'text',
                        data: {id: id, estatus: estatus},
                        success: function (data) {
                            location.reload()

                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            swal("¡Algo salió mal!", {
                                icon: "error",
                            });
                        }
                    });

                } else {

                }
            });

}

$(document).ready(function () {
   $("#btnAgregarMar").click(function () {
        
        let action = $("#action").val();
        let id =$("#id").val();
        let nombre = $("#nombre").val();
        let desc = $("#descripcion").val();
        let descuento = $("#descuento").val();
        if (!nombre ==="" || !desc === "") {
            $.ajax({
                        url: action,
                        type: 'POST',
                        dataType: 'text',
                        data: {id:id, nombre:nombre, descripcion:desc, descuento:descuento},
                        success: function (data) {
                            swal("SUPER CEL MX", "Registro éxitoso", "success");
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            swal("SUPER CEL MX", "Algo salió mal", "error");
                        }
                    });
        } else {
            swal("SUPER CEL MX", "Campo vacio", "warning");
        }
    }) ;
});

//Celular Agregar
$(document).ready(function () {
   $("#btnAgregarCel").click(function () {
        
        let action = $("#action").val();
        let id = $("#id").val();
        let modelo = $("#modelo").val();
        let so = $("#so").val();
        let peso = $("#peso").val();
        let precio = $("#precio").val();
        let bateria = $("#bateria").val();
        let ram = $("#ram").val();
        let almacenamiento = $("#almacenamiento").val();
        let procesador = $("#procesador").val();
        let resolucion = $("#resolucion").val();
        let resDel = $("#resDel").val();
        let resTras = $("#resTras").val();
        let desc = $("#desc").val();
        let descuento = $("#descuento").val();
        let cantidad = $("#cantidad").val();
        let idMarca = $("#idMarca").val();
        let tamano = $("#tamano").val();
        
      
        if (!modelo ==="" || ! so=== ""|| ! precio=== ""|| ! bateria=== ""|| 
                ! ram=== ""|| !almacenamiento=== ""|| !desc=== ""|| !cantidad=== ""|| !tamano==="") {
            $.ajax({
                        url: action,
                        type: 'POST',
                        dataType: 'text',
                        data: {id:id, modelo:modelo, so:so, peso:peso, precio:precio, bateria:bateria,
                        ram:ram,almacenamiento:almacenamiento,procesador:procesador,tamano:tamano ,resolucion:resolucion,
                    resDel:resDel,resTras:resTras,desc:desc,descuento:descuento,cantidad:cantidad,idMarca:idMarca},
                        success: function (data) {
                            swal("SUPER CEL MX", "Registro éxitoso", "success");
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            swal("SUPER CEL MX", "Algo salió mal", "error");
                        }
                    });
        } else {
            swal("SUPER CEL MX", "Campo vacio", "warning");
        }
    }) ;
});


  
