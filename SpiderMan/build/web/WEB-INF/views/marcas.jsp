<%-- 
    Document   : marcas
    Created on : 31/07/2019, 10:24:37 PM
    Author     : ulise
--%>

<br>  
    <!--
    <table border="1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>NOMBRE</th>
                <th>DESCUENTO</th>
                <th>ESTATUS</th>
            </tr>
        </thead>
        <tbody>
            <%--%{alumnos} tiene que ser igual al nombre que aparece en setAttribute--%>
            <c:forEach var="m" items="${marcas}">

                <tr>
                    <td>${m.id}</td>
                    <td>${m.nombre}</td>
                    <td>${m.descuento}</td>
                    <td>${m.estatus}</td>       
                    <td>
                        <input class="btn btn-dark" type="button" value="Editar" name="btnFromEditar" onclick="location.href='ae?${m.id}'"/> 
                        <input class="btn btn-danger" type="button" value="Borrar" name="btnFromEBorrar" onclick='borrar(${m.id})'/> 
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
-->
    <main role="main" class="container">
        <div class="d-flex align-items-center p-3 my-3 text-gray-dark bg-purple rounded shadow-sm">
            <h1> ${titulo}
                <input type="button" class="btn btn-success" value="+" name="btnAgregar" onclick="location.href = 'am'" />
            </h1>
        </div>
        <h6 class="border-bottom border-gray pb-2 mb-0">Marcas activas</h6>


        <c:forEach var="m" items="${marcasOn}">
            <div class="my-3 p-3 bg-white rounded shadow-sm">
                <div class="media text-muted pt-3">
                    <svg class="bd-placeholder-img mr-2 rounded" width="32" height="32" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 32x32"><title>Placeholder</title><rect width="100%" height="100%" fill="#007bff"/><text x="50%" y="50%" fill="#ffffff" dy=".3em">${m.id}</text></svg>
                    <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">${m.nombre}</strong>
                        <td>${m.descripcion}</td>
                        <td>${m.descuento}</td>
                    </p>
                </div>
                <small class="d-block text-right mt-3">
                    <input class="btn btn-light"  type="button" value="Editar" name="btnFromEditar" onclick="location.href = 'em?${m.id}'"/> 
                    <input class="btn" style="background-color: #E76144" type="button" value="Borrar" name="btnFromEBorrar" onclick='borrar2(${m.id},1)'/> 

                </small>
            </div>        
        </c:forEach>
         <h6 class="border-bottom border-gray pb-2 mb-0">Marcas inactivas</h6>


        <c:forEach var="mOff" items="${marcasOff}">
            <div class="my-3 p-3 bg-white rounded shadow-sm">
                <div class="media text-muted pt-3">
                    <svg class="bd-placeholder-img mr-2 rounded" width="32" height="32" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 32x32"><title>Placeholder</title><rect width="100%" height="100%" fill="#E76144"/><text x="50%" y="50%" fill="#ffffff" dy=".3em">${mOff.id}</text></svg>
                    <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">${mOff.nombre}</strong>
                        <td>${mOff.descripcion}</td>
                        <td>${mOff.descuento}</td>
                    </p>
                </div>
                <small class="d-block text-right mt-3">                  
                    <input class="btn" style="background-color: #5FD759" type="button" value="Activar" name="btnFromEBorrar" onclick='borrar2(${mOff.id},0)'/> 

                </small>
            </div>        
        </c:forEach>
    </main>
