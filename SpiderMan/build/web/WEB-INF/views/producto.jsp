<%-- 
    Document   : producto
    Created on : 15/08/2019, 01:22:26 AM
    Author     : ulise
--%>

<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">
    <div class="col-md-5 p-lg-5 mx-auto my-5">
        <h1 class="display-4 font-weight-normal">${c.modelo}</h1>
        <p class="lead font-weight-normal">SISTEMA OPERATIVO: ${c.so}.</p>
        <p class="lead font-weight-normal">PRECIO: $ ${c.precio}.</p>
        <p class="lead font-weight-normal">BATERIA: ${c.bateria}.</p>
        <p class="lead font-weight-normal">RAM: ${c.ram}.</p>
        <p class="lead font-weight-normal">ALMACENAMIENTO:${c.almacenamientoInterno}.</p>
        <p class="lead font-weight-normal">PROCESADOR: ${c.procesador}.</p>
        <p class="lead font-weight-normal">CAMARA DELANTERA: ${c.resolucionCamaraDelatera}.</p>
        <p class="lead font-weight-normal">CAMARA TRASERA: ${c.resolucionCamaraTrasera}.</p>
        <p class="lead font-weight-normal">CANTIDAD: ${c.cantidad}.</p>
        <p class="lead font-weight-normal">DESCRIPCION: ${c.descripcion}.</p>
        <a class="btn btn-outline-secondary" href="#">add car</a>
    </div>
    <div class="product-device shadow-sm d-none d-md-block"></div>
    <div class="product-device product-device-2 shadow-sm d-none d-md-block"></div>
</div>