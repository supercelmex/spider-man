<%-- 
    Document   : marca-form
    Created on : 2/08/2019, 12:45:08 AM
    Author     : ulise
--%>

<div class="container">
    <div class="row">
        <div class="d-flex align-items-center p-3 my-3 text-gray-dark bg-purple rounded shadow-sm col-lg-12">
            <h1> ${titulo} </h1>
        </div>
    </div>
       <div class="d-flex align-items-center p-3 my-3 text-gray-dark bg-purple rounded shadow-sm col-lg-12">
        <form name="marca-form" action="${action}" method="POST">
            
            <input type="hidden" name="action" value="${action}"/>
             <c:if test = "${action == 'em'}">
            <label>ID</label>
            <input type="text" name="id" value="${m.id}" readonly="readonly"/>
            <br>
            </c:if>
             <label>NOMBRE</label>
             <input type="text" name="nombre" value="${m.nombre}" />
            <br>
            <label>DESCRIPCIÓN</label>
            <input type="text" name="descripcion" value="${m.descripcion}" />
            <br>
            <label>DESCUENTO</label>
            <input type="text" name="descuento" value="${m.descuento}" />
            <c:if test = "${error == 'error'}">
            <br>
            <div class="alert alert-danger" role="alert">
               ${errorDesc}
            </div>
            <br>
             </c:if>
            <input type="submit" class="btn btn-success" value="Aceptar" name="btnAceptar"/>
            <!--<input type="button" class="btn btn-success" value="Agregar" name="btnAgregarMar" id="btnAgregarMar"/>-->
            <input type="button"  class="btn btn-warning" value="Cancelar" name="btnCancelar" onclick="location.href='marcas'"/>
       
        <!--</form>-->
       </div>
</div>
            

