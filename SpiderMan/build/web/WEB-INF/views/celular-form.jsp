<%-- 
    Document   : celular-form
    Created on : 6/08/2019, 08:36:40 PM
    Author     : ulise
--%>
<div class="container">
    <div class="row">
        <div class="d-flex align-items-center p-3 my-3 text-gray-dark bg-purple rounded shadow-sm col-lg-12">
            <h1> ${titulo} </h1>
        </div>
    </div>
    <div class="d-flex align-items-center p-3 my-3 text-gray-dark bg-purple rounded shadow-sm col-lg-12">
        <form name="celular-form" action="${action}" method="POST">

            <c:if test = "${action == 'ec'}">
                <label>ID</label>
                <input type="text" name="id"value="${c.id}" readonly="readonly"/>
                <br>
            </c:if>
            <label>MODELO*</label>
            <input type="text" name="modelo" id="modelo" value="${c.modelo}" />
            <br>
            <label>S.O.*</label>
            <input type="text" name="so" id="so" value="${c.so}" />
            <br>
            <label>PESO EN KG</label>
            <input type="text" name="peso" id="peso" value="${c.pesoKg}" />
            <br>
            <label>PRECIO*</label>
            <input type="text" name="precio" id="precio" value="${c.precio}" />
            <br>
            <label>BATERIA*</label>
            <input type="text" name="bateria" id="bateria" value="${c.bateria}" />
            <br>
            <label>RAM*</label>
            <input type="text" name="ram" id="ram" value="${c.ram}" />
            <br>
            <label>ALMACENAMIENTO*</label>
            <input type="text" name="almacenamiento" id="almacenamiento" value="${c.almacenamientoInterno}" />
            <br>
            <label>TAMAÑO PANTALLA</label>
            <input type="text" name="tamano" id="tamano" value="${c.tamañoPantalla}" />
            <br>
            <label>PROCESADOR</label>
            <input type="text" name="procesador" id="procesador" value="${c.procesador}" />

            <br>
            <label>RESOLUCIÓN</label>
            <input type="text" name="resolucion" id="resolucion" value="${c.resolucionPantalla}" />
            <br>
            <label>RES. CAM. DELANTERA</label>
            <input type="text" name="resDel" id="resDel" value="${c.resolucionCamaraDelatera}" />
            <br>
            <label>RES. CAM. TRASERA</label>
            <input type="text" name="resTras" id="resTras" value="${c.resolucionCamaraTrasera}" />
            <br>
            <label>DESCRIPCIÓN*</label>
            <input type="text" name="descripcion" id="desc" value="${c.descripcion}" />
            <br>
            <label>DESCUENTO</label>
            <input type="text" name="descuento" id="descuento" value="${c.descuento}" />
            <br>
            <label>CANTIDAD*</label>
            <input type="text" name="cantidad" id="cantidad" value="${c.cantidad}" />
            <br>
            <label>ID_MARCA</label>
            <select name="idMarca" id="idMarca">
                <c:if test = "${action == 'ec'}">
                    <option value="${c.idMarca.id}" selected>${c.idMarca.nombre}</option>
                </c:if>
                <c:forEach var="m" items="${m}">
                    <option value="${m.id}">${m.nombre}</option>

                </c:forEach>
            </select>
            <br>
            <c:if test = "${error == 'error'}">
                <br>
                <div class="alert alert-danger" role="alert">
                    ${errorDesc}
                </div>
                <br>
            </c:if>

            <input type="submit" class="btn btn-success" value="Aceptar" name="btnAceptar" id="btnAceptar"/>
            <!--<input type="button" class="btn btn-success" value="Agregar" name="btnAgregar" id="btnAgregar"/>-->
            <input type="button"  class="btn btn-warning" value="Cancelar" name="btnCancelar" onclick="location.href = 'celulares'"/>

        </form>
    </div>
</div>
