<%-- 
    Document   : tienda
    Created on : 31/07/2019, 07:10:37 PM
    Author     : ulise
--%>

<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">
    <div class="col-md-5 p-lg-5 mx-auto my-5">
        <h1 class="display-4 font-weight-normal">SUPER CEL "MX"</h1>
        <p class="lead font-weight-normal">Bievenido al proyecto de desarrollo web de octavo semestre�Deseas recibir notificaciones de los nuevos lanzamientos? Registrate.</p>
        <a class="btn btn-outline-secondary" href="#">Registrarse</a>
    </div>
    <div class="product-device shadow-sm d-none d-md-block"></div>
    <div class="product-device product-device-2 shadow-sm d-none d-md-block"></div>
</div>


<main role="main">
    
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="second-slide" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" >
            <div class="container">
              <div class="carousel-caption text-left">
                <h1>�Deseas realizar compras?.</h1>
                <p>Registrate y obten grandes beneficios en la tienda</p>
                <p><a class="btn btn-lg btn-primary" href="#" role="button">Registrarse</a></p>
              </div>
            </div>
          </div>
            <c:forEach var="ms" items="${marcas}">
          <div class="carousel-item">
            <img class="second-slide" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" >
            <div class="container">
              <div class="carousel-caption">
                <h1>${ms.nombre}</h1>
                <p>${ms.descripcion}</p>
                <p><a class="btn btn-lg btn-primary" href="#${ms.nombre}" role="button">Ir a secci�n</a></p>
              </div>
            </div>
          </div>
            </c:forEach>
        
        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>

    
    <c:forEach var="m" items="${marcas}">
        <section class="jumbotron text-center" id="${m.nombre}">
            <div class="container">
                <h1 class="jumbotron-heading">${m.nombre}</h1>
                <p class="lead text-muted">${m.descripcion}</p>

            </div>
        </section>
        <div class="album py-5 bg-light">
            <div class="container">
                <div class="row">
                    <c:forEach var="c" items="${m.celularList}">
                        <div class="col-md-4" >
                            <div class="card mb-4 shadow-sm">
                                <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Thumbnail"><title>Placeholder</title><rect width="100%" height="100%" fill="#55595c"/><text x="50%" y="50%" fill="#eceeef" dy=".3em">${c.modelo}</text></svg>
                                <div class="card-body">
                                    <p class="card-text">${c.descripcion}</p>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-sm btn-outline-secondary" onclick="location.href = 'vc?${c.id}'" >View</button>
                                            <button type="button" class="btn btn-sm btn-outline-secondary">Edit</button>
                                        </div>
                                        <small class="text-muted">$${c.precio}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>

                </div>
            </div>
        </div>
    </c:forEach>
</main>

