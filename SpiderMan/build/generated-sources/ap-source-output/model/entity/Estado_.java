package model.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.entity.Direccion;
import model.entity.Municipio;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-08-15T01:36:56")
@StaticMetamodel(Estado.class)
public class Estado_ { 

    public static volatile SingularAttribute<Estado, Integer> id;
    public static volatile ListAttribute<Estado, Municipio> municipioList;
    public static volatile ListAttribute<Estado, Direccion> direccionList;
    public static volatile SingularAttribute<Estado, String> nombre;

}