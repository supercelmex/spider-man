package model.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.entity.Celular;
import model.entity.Venta;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-08-15T01:36:56")
@StaticMetamodel(DetVen.class)
public class DetVen_ { 

    public static volatile SingularAttribute<DetVen, Integer> id;
    public static volatile SingularAttribute<DetVen, Integer> cantidad;
    public static volatile SingularAttribute<DetVen, Celular> idCelular;
    public static volatile SingularAttribute<DetVen, Venta> idVenta;

}