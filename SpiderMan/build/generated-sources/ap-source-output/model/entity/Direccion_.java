package model.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.entity.Estado;
import model.entity.Usuario;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-08-15T01:36:56")
@StaticMetamodel(Direccion.class)
public class Direccion_ { 

    public static volatile SingularAttribute<Direccion, String> numInt;
    public static volatile SingularAttribute<Direccion, String> numExt;
    public static volatile SingularAttribute<Direccion, Boolean> estatus;
    public static volatile SingularAttribute<Direccion, String> calle;
    public static volatile SingularAttribute<Direccion, Usuario> idUsuario;
    public static volatile SingularAttribute<Direccion, Estado> idMunicipio;
    public static volatile SingularAttribute<Direccion, Integer> id;
    public static volatile SingularAttribute<Direccion, String> cp;
    public static volatile SingularAttribute<Direccion, String> referencia;
    public static volatile SingularAttribute<Direccion, String> colonia;

}