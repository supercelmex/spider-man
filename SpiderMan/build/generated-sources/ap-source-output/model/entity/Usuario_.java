package model.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.entity.Carrito;
import model.entity.Direccion;
import model.entity.Venta;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-08-15T01:36:56")
@StaticMetamodel(Usuario.class)
public class Usuario_ { 

    public static volatile SingularAttribute<Usuario, String> apellido2;
    public static volatile ListAttribute<Usuario, Carrito> carritoList;
    public static volatile SingularAttribute<Usuario, Short> tipo;
    public static volatile SingularAttribute<Usuario, Boolean> estatus;
    public static volatile SingularAttribute<Usuario, String> apellido1;
    public static volatile ListAttribute<Usuario, Venta> ventaList;
    public static volatile SingularAttribute<Usuario, String> correo;
    public static volatile SingularAttribute<Usuario, String> contrasena;
    public static volatile SingularAttribute<Usuario, Integer> id;
    public static volatile SingularAttribute<Usuario, String> nombre2;
    public static volatile ListAttribute<Usuario, Direccion> direccionList;
    public static volatile SingularAttribute<Usuario, String> nombre;

}