package model.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.entity.DetVen;
import model.entity.Marca;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-08-15T01:36:56")
@StaticMetamodel(Celular.class)
public class Celular_ { 

    public static volatile SingularAttribute<Celular, String> descripcion;
    public static volatile SingularAttribute<Celular, Integer> bateria;
    public static volatile SingularAttribute<Celular, String> image;
    public static volatile SingularAttribute<Celular, Float> descuento;
    public static volatile SingularAttribute<Celular, Float> tamañoPantalla;
    public static volatile SingularAttribute<Celular, String> procesador;
    public static volatile SingularAttribute<Celular, Float> resolucionPantalla;
    public static volatile SingularAttribute<Celular, String> modelo;
    public static volatile SingularAttribute<Celular, Float> pesoKg;
    public static volatile SingularAttribute<Celular, Float> resolucionCamaraTrasera;
    public static volatile ListAttribute<Celular, DetVen> detVenList;
    public static volatile SingularAttribute<Celular, Float> precio;
    public static volatile SingularAttribute<Celular, Float> resolucionCamaraDelatera;
    public static volatile SingularAttribute<Celular, Boolean> estatus;
    public static volatile SingularAttribute<Celular, Integer> almacenamientoInterno;
    public static volatile SingularAttribute<Celular, Integer> id;
    public static volatile SingularAttribute<Celular, Integer> cantidad;
    public static volatile SingularAttribute<Celular, String> so;
    public static volatile SingularAttribute<Celular, Marca> idMarca;
    public static volatile SingularAttribute<Celular, Integer> ram;

}